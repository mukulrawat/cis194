module Lec1 where
import Test.HUnit

-- Haskell is a lazy functional programming language
-- Haskell is
-- (1) Functional
-- (2) Pure
-- (3) Lazy

-- Course focuses on three main themes
-- (1) Types
-- (2) Abstraction
-- (3) Wholemeal programming


-- Declarations and variables

x :: Int
x = 3

y :: Int
y = y + 1

-- Basic Types
--
-- Machine-sized Integers
i :: Int
i = -78

biggestInt, smallestInt :: Int
biggestInt = maxBound
smallestInt = minBound

-- The integer type, on the other hand is limited but the amount
-- of memory on your machine

-- Arbitrary-precision integers
n :: Integer
n = 1234567890987654321987340982334987349874534

reallyBig :: Integer
reallyBig = 2^(2^(2^(2^2)))

numDigits :: Int
numDigits = length (show reallyBig)

-- Double-precision floating point
d1, d2 :: Double
d1 = 4.5387
d2 = 6.2831e-4

-- Booleans
b1, b2 :: Bool
b1 = True
b2 = False

-- Unicode characters
c1, c2, c3 :: Char
c1 = 'x'
c2 = 'y'
c3 = 'z'

-- Strings are lists of characters with special syntax
s :: String
s = "Hello, Haskell!"

-- Defining basic functions
--
-- Compute the sum of integers from 1 to n
sumtorial :: Integer -> Integer
sumtorial 0 = 0
sumtorial n = n + sumtorial (n-1)

-- Examples of guards
hailstone :: Integer -> Integer
hailstone n
  | n `mod` 2 == 0 = n `div` 2
  | otherwise     = 3*n + 1

-- another example of a function
foo :: Integer -> Integer
foo 0 = 16
foo 1
  | "Haskell" > "C++" = 3
  | otherwise         = 4
foo n
  | n < 0          = 0
  | n `mod` 17 == 2 = -43
  | otherwise      = n + 3

isEven :: Integer -> Bool
isEven n
  | n `mod` 2 == 0 = True
  | otherwise     = False

-- Pairs

p :: (Int, Char)
p = (3, 'x')

-- elements of pair can be extracted again with pattern matching
sumPair :: (Int, Int) -> Int
sumPair (x,y) = x + y

-- Using functions, and multiple arguments

f :: Int -> Int -> Int -> Int
f x y z = x + y + z
ex17 = f 3 17 8

-- Lists

nums, range, range2 :: [Integer]
nums = [1,2,3,19]
range = [1..100]
range2 = [2,4,..100]

hello1 :: [Char]
hello1 = ['h', 'e', 'l', 'l', 'o']

hello2 :: string
hello2 = "hello"

-- Constructing lists

emptyList = []

ex18 = 1 : []
ex19 = 3 : (1 : [])
ex20 = 2 : 3 : 4 : []

-- Generate the sequence of hailstone iterations from a starting number
hailstoneSeq :: Integer -> [Integer]
hailstoneSeq 1 = [1]
hailstoneSeq n = n : hailstoneSeq (hailstone n)

-- Functions on lists

-- Compute the length of a list of integers
intListLength :: [Integer] -> Integer
intListLength []     = 0
intListLength (_:xs) = 1 + intListLength xs

sumEveryTwo :: [Integer] -> [Integer]
sumEveryTwo []         = []
sumEveryTwo (x:[])     = [x]
sumEveryTwo (x:(y:zs)) = (x+y) : sumEveryTwo zs

-- Combining functions
-- The number of hailstone steps needed to reach 1 from a
-- starting number
hailstoneLen :: Integer -> Integer
hailstoneLen n = intListLength (hailstoneSeq n) - 1



