{-
name: mukul rawat
notes:
-}

module HW01 where
import Test.HUnit

isThisWorking :: String
isThisWorking = "Yes"

-------------------------------------------------------------------
-- Exercise 1
-------------------------------------------------------------------

-- Function that breaks up a number into its last digit
lastDigit :: Integer -> Integer
lastDigit n = n `mod` 10

-- Define unit test for lastDigit
lastDtest :: Test
lastDtest = TestList [
                      lastDigit 123 ~?= 3,
                      lastDigit 0   ~?= 0
                      ]

-- Run the test
runDTest :: IO Counts
runDTest = runTestTT lastDtest

-- Function that breaks up a number into a number excluding last digit
dropLastDigit :: Integer -> Integer
dropLastDigit = \n -> (n - lastDigit n ) `div` 10

-- Define unit test for dropLastDigit
dropLtest :: Test
dropLtest = TestList [
                      dropLastDigit 123 ~?= 12,
                      dropLastDigit 5   ~?= 0
                      ]

-- Run the test
rundropTest :: IO Counts
rundropTest = runTestTT dropLtest


